FROM jboss/keycloak:latest

ARG KEYCLOAK_PASSWORD
ARG POSTGRES_PASSWORD

# Add Apple IDP Integration
ADD ./apple-idp/apple-idp.jar $JBOSS_HOME/standalone/deployments/

# Import Keycloak realm/users
RUN mkdir /opt/jboss/keycloak/realm-config
COPY ./realm-config /opt/jboss/keycloak/realm-config

# Keycloak config
# ENV KEYCLOAK_LOGLEVEL DEBUG
# ENV ROOT_LOGLEVEL DEBUG
ENV KEYCLOAK_IMPORT /opt/jboss/keycloak/realm-config/core-realm.json
ENV PROXY_ADDRESS_FORWARDING true

# Keycloak users
ENV KEYCLOAK_USER admin
ENV KEYCLOAK_PASSWORD upwork

# Database config
ENV DB_VENDOR postgres
ENV DB_ADDR 10.51.208.3
ENV DB_DATABASE keycloak
ENV DB_USER keycloak
ENV DB_PASSWORD $POSTGRES_PASSWORD

# Runtime config
ENV JAVA_OPTS -server \
    -Xms256m \
    -Xmx1024m \
    -XX:MetaspaceSize=96M \
    -XX:MaxMetaspaceSize=256m \
    -Djava.net.preferIPv4Stack=true \
    -Djava.awt.headless=true \
    -Djboss.bind.address=0.0.0.0 \
    -Djboss.modules.system.pkgs=org.jboss.byteman \
    -Dkeycloak.migration.strategy=OVERWRITE_EXISTING \
    -Dkeycloak.profile.feature.upload_scripts=enabled 
    
EXPOSE 8080